import { useCallback, useReducer } from "react";

export type StateType = {
  inputs: Record<string, InputType>;
  isValid: boolean;
};

type ActionType =
  | {
      type: "INPUT_CHANGE";
      value: string | number;
      inputId: string;
      isValid: boolean;
    }
  | {
      type: "SET_DATA";
      inputs: Record<string, InputType>;
      formIsValid: boolean;
    };

export type InputType = {
  id?: string;
  isValid: boolean;
  value: string | number;
};

const formReducer = (state: StateType, action: ActionType) => {
  switch (action.type) {
    case "INPUT_CHANGE":
      let formIsValid = true;
      for (const inputId in state.inputs) {
        if (!state.inputs[inputId]) {
          continue;
        }
        if (inputId === action.inputId) {
          formIsValid = formIsValid && action.isValid;
        } else {
          formIsValid = formIsValid && state.inputs[inputId].isValid;
        }
      }
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.inputId]: { value: action.value, isValid: action.isValid },
        },
        isValid: formIsValid,
      };
    case "SET_DATA":
      return {
        inputs: action.inputs,
        isValid: action.formIsValid,
      };
    default:
      return state;
  }
};

export const useForm = (initialInputs: Record<string, InputType>, initialFormValidity: boolean) => {
  const [formState, dispatch] = useReducer(formReducer, {
    inputs: initialInputs,
    isValid: initialFormValidity,
  });

  const inputHandler: (id: string, value: string | number, isValid: boolean) => void = useCallback(
    (id: string, value: string | number, isValid: boolean) => {
      dispatch({
        type: "INPUT_CHANGE",
        value: value,
        isValid: isValid,
        inputId: id,
      });
    },
    []
  );

  const setFormData: (inputs: Record<string, InputType>, formIsValid: boolean) => void = useCallback(
    (inputData: Record<string, InputType>, formValidity: boolean) => {
      dispatch({
        type: "SET_DATA",
        inputs: inputData,
        formIsValid: formValidity,
      });
    },
    []
  );

  return {formState, inputHandler, setFormData};
};

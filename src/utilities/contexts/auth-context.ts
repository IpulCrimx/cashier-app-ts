import { createContext } from "react";

export interface IAuthContext {
  isLoggedIn: boolean;
  isAdmin: boolean;

  login: (success: boolean, isAdmin: boolean) => void;
  logout: () => void;
}

export const AuthContext = createContext<IAuthContext>({
  isLoggedIn: false,
  isAdmin: false,

  logout: () => {},
  login: () => {},
});

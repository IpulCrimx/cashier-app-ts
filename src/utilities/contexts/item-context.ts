import { createContext, useContext } from "react";

export type IItemContext = {
  name: string;
  desc?: string;
  price: number;
  stock: number;

  changeStock?: (newStock: number) => void;
  changePrice?: (newPrice: number) => void;
};

export type ContextItem = {
  items: IItemContext[];
  setItems: (items: IItemContext[]) => void;
};

export const ItemContext = createContext<ContextItem>({
  items: [],
  setItems: (items: IItemContext[]) => {},
});

export const useItemContext = () => useContext(ItemContext);

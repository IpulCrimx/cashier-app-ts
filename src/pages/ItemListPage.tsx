import React from 'react';
import ItemTable from '../components/ItemList/ItemTable';
import OpenToolbar from '../components/ItemList/OpenToolbar';

const ItemListPage = () => {
  return (
    <div className="px-8 py-4 bg-gray-50">
      <div className="bg-gray-100" style={{minHeight: '60vh'}}>
        <OpenToolbar />
        <ItemTable />
      </div>
    </div>
  );
};

export default ItemListPage;
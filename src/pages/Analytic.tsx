import React from "react";

import AnalyticLog from "../components/Analytics/AnalyticLog";
import LowItemInfo from "../components/Analytics/LowItemInfo";

const Analytic: React.FC = () => {
  return (
    <div className="grid gap-12 grid-cols-2 my-6 px-8">
      <AnalyticLog />
      <LowItemInfo />
    </div>
  );
};

export default Analytic;

import React from "react";

const Homepage = () => {
  const styles = ["mt-4", "text-xl", "font-bold", 'track-wider'];

  return <div className={styles.join(" ")}>This is homepage...</div>;
};

export default Homepage;

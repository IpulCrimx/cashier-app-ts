import React, { useContext } from "react";
import Button from "../components/shared/UIElement/FormElements/Button";
import Card from "../components/shared/UIElement/Card";
import { useForm } from "../utilities/hooks/form-hooks";
import Input from "../components/shared/UIElement/FormElements/Input";
import { AuthContext } from "../utilities/contexts/auth-context";

const Login = () => {
  const { login } = useContext(AuthContext);
  const { formState, inputHandler } = useForm(
    {
      username: {
        value: "",
        isValid: false,
      },
      password: {
        value: "",
        isValid: false,
      },
    },
    false
  );

  const cardStyles = [
    "bg-yujin-blue",
    "w-4/6 h-4/6",
    "mt-6",
    "absolute",
    "left-1/2",
    "transform -translate-x-1/2",
    "justify-center",
    "shadow-xl",
    "px-4 py-2",
  ];

  const inputStyle = ["w-full", "mb-2", "px-2 py-1", "rounded-sm"];

  const onLoginHandler = () => {
    login(true, true);
  };

  return (
    <Card cssClasses={cardStyles}>
      <h4 className="text-3xl text-center font-bold my-4">Login</h4>
      <div className="block relative mt-2 px-4 w-full ">
        <Input
          inputClasses={inputStyle}
          id="username"
          label="Username"
          element="input"
          type="text"
          onInput={inputHandler}
          isValid={formState.inputs.username.isValid}
          value={formState.inputs.username.value + ""}
          errorText="Please input valid username"
        />
        <Input
          inputClasses={inputStyle}
          id="password"
          label="Password"
          element="input"
          type="password"
          onInput={inputHandler}
          isValid={formState.inputs.password.isValid}
          value={formState.inputs.password.value + ""}
          errorText="Please input valid username"
        />
        <Button
          cssClasses={[
            "float-right",
            "px-3 py-1",
            "rounded-md",
            "bg-yujin-blue-hover text-slate-50",
            "active:text-gray-200",
            "hover:text-gray-200",
            "focus:text-gray-200",
          ]}
          onClick={onLoginHandler}
        >
          Login
        </Button>
      </div>
      <div className="fixed bottom-0 right-0 text-right mx-8 mb-4 text-xs">
        <p>
          Doesn't have an account?{" "}
          <a href="#" className="cursor-pointer">
            Please tell your admin to create an account for you.
          </a>
        </p>
      </div>
    </Card>
  );
};

export default Login;

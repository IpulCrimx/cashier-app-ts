import { useCallback, useReducer } from "react";

import { IItemContext, useItemContext } from "../utilities/contexts/item-context";
import TopBar from "../components/Pay/TopBar";
import Result from "../components/Pay/Result";
import TableParent from "../components/Pay/TableParent";
import AvailableItems from "../components/Pay/AvailableItems/AvailableItems";

// types and interfaces
export interface RegisteredItemProps {
  name: string;
  price: number;
  quantity?: number;
  category?: string | string[];
}

interface ItemState {
  items: RegisteredItemProps[];
  totalCost?: number;
  customerPay?: number;
}

type ItemPayloadType = {
  item?: RegisteredItemProps;
  itemName?: string;
  pay?: number;
};

interface ItemAction {
  type:
    | ItemActionType.ADD
    | ItemActionType.REMOVE
    | ItemActionType.CHANGE
    | ItemActionType.TOTAL
    | ItemActionType.ADD_PAY
    | ItemActionType.RESET;
  payload: ItemPayloadType;
}

enum ItemActionType {
  ADD = "ADD",
  REMOVE = "REMOVE",
  CHANGE = "CHANGE",
  TOTAL = "TOTAL",
  ADD_PAY = "ADD_PAY",
  PAY = "PAY",
  RESET = "RESET",
}

const registeredItemReducer = (state: ItemState, action: ItemAction): ItemState => {
  const { type, payload } = action;
  let totalCost = 0;

  switch (type) {
    case ItemActionType.ADD:
      if (payload.item) {
        const addedItems = [...state.items, payload.item];
        addedItems.forEach((item) => (totalCost += item.price * item.quantity!));

        return {
          ...state,
          items: addedItems,
          totalCost,
        };
      } else {
        console.warn("Item in payload is empty!");
        return state;
      }
    case ItemActionType.CHANGE:
      const item = state.items.find((x) => x.name === payload.item?.name);

      if (!item) {
        console.warn("Item in payload not found!");
        return state;
      }

      const itemIndex = state.items.indexOf(item!);
      if (itemIndex < 0) {
        console.warn("Item in payload not found!");
        return state;
      }

      const newItems =
        payload.item?.quantity! > 0
          ? [
              ...state.items.slice(0, itemIndex),
              {
                name: item.name,
                category: item.category,
                quantity: payload.item?.quantity,
                price: item.price,
              },
              ...state.items.slice(itemIndex + 1),
            ]
          : // if the quantity is equal or less than zeros
            state.items.filter((it) => it.name !== payload.item?.name);

      newItems.forEach((item) => (totalCost += item.price * item.quantity!));

      return {
        ...state,
        items: newItems,
        totalCost,
      };

    case ItemActionType.REMOVE:
      const updatedItems = [...state.items.filter((x) => x.name !== payload.itemName)];

      updatedItems.forEach((item) => (totalCost += item.price * item.quantity!));

      return {
        ...state,
        items: updatedItems,
        totalCost,
      };
    case ItemActionType.TOTAL:
      return state;
    case ItemActionType.ADD_PAY:
      const newState = {
        ...state,
        customerPay: action.payload.pay!,
      };

      return newState;
    case ItemActionType.RESET:
      return {
        items: [],
        customerPay: 0,
        totalCost: 0,
      };
    default:
      return state;
  }
};

const Pay = () => {
  const [itemState, dispatch] = useReducer(registeredItemReducer, {
    items: [],
  });
  const { items, setItems } = useItemContext();

  // handlers area
  const onItemHandler = (item: IItemContext) => {
    const foundItem = itemState.items.find((i) => i.name === item.name);
    if (foundItem) {
      dispatch({
        type: ItemActionType.CHANGE,
        payload: {
          item: {
            name: foundItem.name,
            price: foundItem.price,
            quantity: Math.min(foundItem.quantity! + 1, item.stock),
          },
        },
      });
    } else {
      dispatch({
        type: ItemActionType.ADD,
        payload: {
          item: {
            name: item.name,
            price: item.price,
            quantity: 1,
          },
        },
      });
    }
  };

  const onChangeItemHandler = useCallback(
    (itemStock: number, itemName: string, price: number, quantity: number) => {
      const updated: RegisteredItemProps = {
        name: itemName,
        price,
        quantity: Math.min(quantity, itemStock),
      };

      dispatch({
        type: ItemActionType.CHANGE,
        payload: { item: updated },
      });
    },
    []
  );

  const onRemoveItemHandler = useCallback((itemName: string) => {
    dispatch({
      type: ItemActionType.REMOVE,
      payload: { itemName },
    });
  }, []);

  const onConfirmPaymentHandler = (pay: number) => {
    dispatch({ type: ItemActionType.ADD_PAY, payload: { pay } });
  };

  const onFinishPaymentHandler = () => {
    const newItems = [];

    for (let i = 0; i < items.length; i++) {
      const it = items[i];
      for (let j = 0; j < itemState.items.length; j++) {
        if (it.name === itemState.items[j].name) {
          it.stock = it.stock - itemState.items[j].quantity!;
          it.stock = Math.max(0, it.stock);

          break;
        }
      }

      newItems.push(it);
    }

    setItems(newItems);
    dispatch({ type: ItemActionType.RESET, payload: {} });
  };
  return (
    <div className="container mx-auto box-border mt-4 pt-2 px-6 lg:w-[70%] lg:mx-0">
      <TopBar />
      <div className="lg:w-3/10 lg:fixed lg:top-12 lg:right-0 lg:mt-4">
        <TableParent
          registeredItem={itemState.items}
          onRemoveHandler={onRemoveItemHandler}
          onChangeHandler={(itemName: string, itemPrice: number, qt: number) => {
            const item = items.find((it) => it.name === itemName);
            if (!item) console.error("Invalid item name");

            onChangeItemHandler(item!.stock, itemName, itemPrice, qt);
          }}
        />
        <Result
          total={itemState.totalCost ? itemState.totalCost : 0}
          onConfirmPaymentHandler={onConfirmPaymentHandler}
          onFinishPaymentHandler={onFinishPaymentHandler}
        />
      </div>
      <AvailableItems items={items} onChildItemClick={onItemHandler} />
    </div>
  );
};

export default Pay;

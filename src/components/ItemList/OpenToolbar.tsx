import { PencilAltOutline, Plus, X } from 'heroicons-react';
import React from 'react';

const OpenToolbar = () => {
  const buttonStyles = [
    "w-28 h-28",
    "rounded-md",
    "box-border",
    "text-slate-50",
    "text-lg text-center items-center",
    "my-4 mr-4",
    "px-6 py-2",
  ];

  const blueButton = [
    "bg-yujin-blue",
    "hover:bg-yujin-blue-hover",
    "focus:bg-yujin-blue-hover",
    "active:bg-yujin-blue-hover ",
  ];

  const redButton = [
    "bg-yujin-red",
    "hover:bg-yujin-red-hover",
    "focus:bg-yujin-red-hover",
    "active:bg-yujin-red-hover ",
  ];

  return (
    <div className="w-full flex flex-row mb-4">
      <div>
        <h1 className={`${blueButton.join(" ")} ${buttonStyles.join(" ")}`}>
          <Plus className="mx-auto" size={52} />
          <p className="pt-3">Tambah</p>
        </h1>
      </div>
      <div>
        <h1 className={`${blueButton.join(" ")} ${buttonStyles.join(" ")}`}>
          <PencilAltOutline className="mx-auto" size={52} />
          <p className="pt-3">Ubah</p>
        </h1>
      </div>
      <div>
        <h1 className={`${redButton.join(" ")} ${buttonStyles.join(" ")}`}>
          <X className="mx-auto" size={52} />
          <p className="pt-3">Hapus</p>
        </h1>
      </div>
    </div>
  );
};

export default OpenToolbar;
import React from 'react';
import dummy_items from './dummy_items.json';
import ItemChild from './ItemChild';

const ItemTable = () => {
  const labelStyle = ["m-0 mr-4"];

  const dropdownStyle = ["m-0 w-3/4", "px-4", "border border-gray-400"];

  const searchFieldStyle = ["m-0 w-3/4", "px-2", "border border-gray-400"];

  return (
    <div>
      <div className="grid grid-cols-3 gap-4">
        <div>
          <label className={labelStyle.join(" ")}>Filter</label>
          <select className={dropdownStyle.join(" ")}>
            <option>Kategori</option>
            <option>Nama Item</option>
          </select>
        </div>
        <div>
          <label className={labelStyle.join(" ")}>Input form</label>
          <select className={dropdownStyle.join(" ")}>
            <option>Terbaru</option>
            <option>Terlama</option>
            <option>Stok Terbanyak</option>
            <option>Stok Paling Sedikit</option>
          </select>
        </div>
        <div className="justify-between">
          <label className={labelStyle.join(" ")}>Search</label>
          <input
            type="text"
            placeholder="Search item..."
            className={searchFieldStyle.join(" ")}
          />
        </div>
      </div>

      <table className="w-full table-auto border-collapse border border-gray-700 mt-8 mb-6">
        <thead>
          <tr>
            <th className="bg-orange-200 border border-gray-700">Kategori</th>
            <th className="bg-orange-200 border border-gray-700">Name</th>
            <th className="bg-orange-200 border border-gray-700">Harga</th>
            <th className="bg-orange-200 border border-gray-700">Stok</th>
          </tr>
        </thead>
        <tbody>
          {dummy_items.map((item, index) => (
            <ItemChild
              key={`item--${item.name}--${index}`}
              tags={item.tags}
              name={item.name}
              price={item.price}
              stock={item.stock}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ItemTable;
import React from "react";

interface IChildProps {
  tags: string | string[] | undefined | null;
  name: string;
  price: number;
  stock: number;
}

const ItemChild: React.FC<IChildProps> = (props) => (
  <tr>
    <td className=" border border-gray-700 px-2">{props.tags}</td>
    <td className=" border border-gray-700 px-4">{props.name}</td>
    <td className=" border border-gray-700 px-2 text-right">{props.price}</td>
    <td className=" border border-gray-700 px-2 text-right">{props.stock}</td>
  </tr>
);

export default ItemChild;

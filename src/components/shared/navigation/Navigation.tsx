import React, { useState } from "react";
import { Link } from "react-router-dom";
import Backdrop from "../UIElement/Backdrop";
import SideDrawer from "../UIElement/SideDrawer";
import MainHeader from "./MainHeader";
import NavLinks from "./NavLinks";

const Navigation = () => {
  const [isDrawerOpen, setDrawerOpen] = useState<boolean>(false);

  const drawerOpenHandler = () => {
    setDrawerOpen(true);
  };

  const drawerCloseHandler = () => {
    setDrawerOpen(false);
  };

  return (
    <>
      {isDrawerOpen && <Backdrop onClick={drawerCloseHandler} />}
      <SideDrawer show={isDrawerOpen} closeDrawer={drawerCloseHandler}>
        {isDrawerOpen ? (
          <>
            <div className="font-bold tracking-widest text-4xl mx-8- mt-6 text-slate-50">
              LOGO
            </div>
            <hr className="mx-8 mt-4" />
            <nav className="h-full">
              <NavLinks cssClasses={["flex-col", "justify-start", "pt-8"]} />
            </nav>
          </>
        ) : null}
      </SideDrawer>
      <MainHeader>
        <button
          className="w-8 h-8 bg-transparent border-none flex flex-col mr-8 justify-around cursor-pointer lg:hidden"
          onClick={drawerOpenHandler}
        >
          <span className="block w-12 h-1 bg-white"></span>
          <span className="block w-12 h-1 bg-white"></span>
          <span className="block w-12 h-1 bg-white"></span>
        </button>

        <Link to="/">
          <h1 className="text-slate-50 decoration-none lg:ml-10">Title</h1>
        </Link>

        <nav className="hidden lg:block">
          <NavLinks cssClasses={["justify-center"]} />
        </nav>
      </MainHeader>
    </>
  );
};

export default Navigation;

import React, { FC } from "react";

interface IMainHeaderProps {
  cssClasses?: string[];
}

const MainHeader: FC<IMainHeaderProps> = (props) => {
  const styles = [
    "w-full",
    "h-16",
    "flex",
    "items-center",
    "fixed",
    "top-0 left-0",
    "bg-yujin-blue",
    "shadow-md",
    "px-2",
    "z-10",
    "lg:justify-between",
  ];

  if (props.cssClasses) styles.push(...props.cssClasses);

  return <header className={styles.join(" ")}>{props.children}</header>;
};

export default MainHeader;

import React, { useContext, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { Template, ChartBar, Calculator, PencilAltOutline, CogOutline, LogoutOutline } from "heroicons-react";

import { AuthContext } from "../../../utilities/contexts/auth-context";
import Button from "../UIElement/FormElements/Button";

interface INavlinksProps {
  cssClasses: string[];
}

const NavLinks: React.FC<INavlinksProps> = (props) => {
  const { isLoggedIn, isAdmin, logout } = useContext(AuthContext);

  const listStyle = ["list-none", "flex", "items-left lg:items-center", "m-0 ml-4", "p-0", "h-full"];
  const listItemStyle = [
    "styling-none",
    "m-1 mr-6 lg:mr-1",
    "py-2 px-3",
    "text-slate-50",
    "cursor-pointer",
    "active:text-yujin-blue hover:font-bold lg:active:font-normal active:bg-slate-50 active:rounded-lg active:shadow-sm",
    "hover:text-yujin-blue hover:font-bold lg:hover:font-normal hover:bg-slate-50 hover:rounded-lg hover:shadow-sm",
    "focus:outline-none",
  ];

  if (props.cssClasses) {
    listStyle.push(...props.cssClasses);
  }

  return (
    <ul className={listStyle.join(" ")}>
      <li className={listItemStyle.join(" ")}>
        <NavLink to="/">
          <Template className=" lg:hidden mr-2 inline-block" />
          Dashboard
        </NavLink>
      </li>
      {isLoggedIn && (
        <li className={listItemStyle.join(" ")}>
          <NavLink to="/main">
            <Calculator className=" lg:hidden mr-2 inline-block" />
            Kasir
          </NavLink>
        </li>
      )}
      {isLoggedIn && (
        <li className={listItemStyle.join(" ")}>
          <NavLink to="items">
            <PencilAltOutline className=" lg:hidden mr-2 inline-block" />
            Stock Barang
          </NavLink>
        </li>
      )}
      {isLoggedIn && isAdmin && (
        <li className={listItemStyle.join(" ")}>
          <NavLink to="graph">
            <ChartBar className=" lg:hidden mr-2 inline-block" />
            Analisis
          </NavLink>
        </li>
      )}
      {isLoggedIn && (
        <li className={listItemStyle.join(" ")}>
          <NavLink to="setting">
            <CogOutline className=" lg:hidden mr-2 inline-block" />
            Setting
          </NavLink>
        </li>
      )}
      {!isLoggedIn && (
        <li className={listItemStyle.join(" ")}>
          <NavLink to="login">
            <LogoutOutline className="lg:hidden mr-2 inline-block" />
            Login
          </NavLink>
        </li>
      )}
      {isLoggedIn && (
        <li className={listItemStyle.join(" ")}>
          <Button onClick={logout}>
            <LogoutOutline className=" lg:hidden mr-2 inline-block" />
            Logout
          </Button>
        </li>
      )}
    </ul>
  );
};

export default NavLinks;

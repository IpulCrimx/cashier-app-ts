import React, { FC } from 'react';
import reactDom from "react-dom";

interface ISideDrawerProps{
  show: boolean,
  closeDrawer: () => void,
  cssClasses?: string[]
}

const SideDrawer:FC<ISideDrawerProps> = (props) => {
  const drawerStyle = [
    "fixed",
    "top-0 left-0",
    "w-2/5",
    "bg-yujin-blue",
    "shadow-lg",
    "z-50",
    "lg:hidden",
    "duration-100",
  ];
  
  if (props.show) drawerStyle.push("ease-in", "translate-x-0");
  else drawerStyle.push("ease-out", "-translate-x-full");
  
  const content = (
    <aside
      className={drawerStyle.join(" ")}
      style={{ height: "100vh" }}
      onClick={props.closeDrawer}
    >
      {props.children}
    </aside>
  );

  return reactDom.createPortal(content, document.getElementById("drawer-hook")!);
};

export default SideDrawer;
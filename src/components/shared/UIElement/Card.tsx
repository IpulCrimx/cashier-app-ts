import React from "react";

interface ICardProps {
  cssClasses?: string[];

  onClick?: () => void;
}

const Card: React.FC<ICardProps> = (props) => {
  const cssClasses = ['bg-slate-50', "rounded-lg", "overflow-hidden"];

  if (props.cssClasses) {
    cssClasses.push(...props.cssClasses);
  }

  return (
    <div className={cssClasses.join(" ")} onClick={props.onClick}>
      {props.children}
    </div>
  );
};

export default Card;

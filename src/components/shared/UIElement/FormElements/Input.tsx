import React, { ChangeEvent, useEffect, useReducer } from "react";
import { InputType } from "../../../../utilities/hooks/form-hooks";

type InputProps = {
  id: string;
  type: string;
  label: string;
  value: string;
  isValid: boolean;
  element: "input" | "textarea";
  validators?: Record<string, unknown>[];
  onInput: (id: string, value: string, isValid: boolean) => void;
  checkValidation?: (val: number) => void;

  initialValue?: string;
  initialValid?: boolean;

  row?: number;
  placeholder?: string;
  errorText?: string;

  cssClasses?: string[];
  inputClasses?: string[];
};

type InputStateType = InputType & {
  isTouched: boolean;
};

type InputActionType =
  | {
      type: "CHANGE";
      val: string;
      validators?: Record<string, unknown>[];
    }
  | {
      type: "TOUCH";
    };

const inputReducer = (state: InputStateType, action: InputActionType) => {
  switch (action.type) {
    case "CHANGE":
      return {
        ...state,
        value: action.val,
        isValid: true,
      };
    case "TOUCH": {
      return {
        ...state,
        isTouched: true,
      };
    }
    default:
      return state;
  }
};

const Input: React.FC<InputProps> = (props) => {
  const [inputState, dispatch] = useReducer(inputReducer, {
    id: "",
    value: props.initialValue || "",
    isTouched: false,
    isValid: props.initialValid || false,
  });

  // css area
  const cssClasses = [];
  const cssInputClasses = [
    "px-2 lg:ml-4 lg:py-1",
    "border-2 border-yujin-blue",
    "inline-block",
    "hover:border-yujin-blue active:border-yujin-blue",
    "appearance-none",
  ];

  if (props.cssClasses) cssClasses.push(...props.cssClasses);
  if (props.inputClasses) cssInputClasses.push(...props.inputClasses);

  const { id, onInput } = props;
  const { value, isValid } = inputState;

  useEffect(() => {
    onInput(id, value + "", isValid);
  }, [id, value, isValid, onInput]);

  const onChangeHandler = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    dispatch({ type: "CHANGE", val: event.target.value, validators: props.validators });
  };

  const onTouchHandler = () => {
    dispatch({ type: "TOUCH" });
  };

  const element =
    props.element === "input" ? (
      <input
        className={cssInputClasses.join(" ")}
        id={props.id}
        type={props.type}
        placeholder={props.placeholder}
        onChange={onChangeHandler}
        onBlur={onTouchHandler}
        value={inputState.value}
      />
    ) : (
      <textarea
        className={cssInputClasses.join(" ")}
        id={props.id}
        rows={props.row || 3}
        onChange={onChangeHandler}
        onBlur={onTouchHandler}
        value={inputState.value}
      />
    );

  return (
    <div className={cssClasses.join(" ")}>
      <label htmlFor={props.id}>{props.label}</label>
      {element}
      {!inputState.isValid && inputState.isTouched && <p>{props.errorText}</p>}
    </div>
  );
};

export default Input;

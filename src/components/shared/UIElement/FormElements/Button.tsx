import React from "react";

interface IButtonProps {
  onClick: () => void;
  cssClasses?: string[];
}

const Button: React.FC<IButtonProps> = props => {
  const styles = [];

  if (props.cssClasses) styles.push(...props.cssClasses);
  else
    styles.push(
      "bg-yujin-blue-hover text-slate-50",
      "active:text-gray-200",
      "hover:text-gray-200",
      "focus:text-gray-200",
      "px-4 py-2",
      "mt-2",
      "rounded-md"
    );

  return (
    <button className={styles.join(" ")} onClick={props.onClick}>
      {props.children}
    </button>
  );
};

export default Button;

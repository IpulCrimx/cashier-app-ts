import React, { FC } from "react";
import reactDom from "react-dom";

interface IBackdropProps {
  cssClasses?: string[];
  onClick?: () => void;
}

const Backdrop: FC<IBackdropProps> = (props) => {
  const styles: string[] = [
    "fixed",
    "top-0 left-0",
    "w-full h-full",
    "bg-black opacity-75",
    "z-50",
  ];
  if (props.cssClasses) styles.push(...props.cssClasses);

  const content = (
    <div className={styles.join(" ")} onClick={props.onClick}></div>
  );

  return reactDom.createPortal(
    content,
    document.getElementById("backdrop-hook")!
  );
};

export default Backdrop;

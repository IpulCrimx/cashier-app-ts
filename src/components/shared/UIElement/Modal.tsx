import React, { CSSProperties, FormEventHandler } from "react";
import reactDOM from "react-dom";

import Backdrop from "./Backdrop";
import Card from "./Card";

interface IModalProps {
  styles?: CSSProperties;
  cssClasses?: string[];
  cssHeader?: string[];
  cssContent?: string[];
  cssFooter?: string[];

  header?: string;
  footer?: any;

  show: boolean;

  onSubmit?: () => void;
  onCancel: () => void;
}

const ModalOverlay: React.FC<IModalProps> = (props) => {
  const cssClasses = ["duration-200", 'absolute', 'top-1/4 left-1/2', 'transform', '-translate-x-1/2 -translate-y-1/2', 'z-50'];
  const cssHeaderClasses = [];
  const cssContentClasses = [];
  const cssFooterClasses = [];

  if (props.cssClasses) cssClasses.push(...props.cssClasses);
  if (props.cssHeader) cssHeaderClasses.push(...props.cssHeader);
  if (props.cssContent) cssContentClasses.push(...props.cssContent);
  if (props.cssFooter) cssFooterClasses.push(...props.cssFooter);

  if (props.show) cssClasses.push("ease-out", "translate-y-0");
  else cssClasses.push("ease-in", "translate-y-full");

  const content = (
    <div className={cssClasses.join(" ")} style={props.styles}>
      <Card cssClasses={['bg-slate-50', 'p-4', 'max-w-2/5']}>
        <header className={cssHeaderClasses.join(" ")}>
          <h2>{props.header}</h2>
        </header>
        <form
          onSubmit={
            props.onSubmit ? props.onSubmit : (event) => event.preventDefault()
          }
        >
          <div className={cssContentClasses.join(" ")}>{props.children}</div>
          <footer className={cssFooterClasses.join(" ")}>{props.footer}</footer>
        </form>
      </Card>
    </div>
  );

  return reactDOM.createPortal(content, document.getElementById("modal-hook")!);
};

const Modal: React.FC<IModalProps> = (props) => {
  return (
    <>
      {props.show && <Backdrop onClick={props.onCancel} />}

      <ModalOverlay {...props} />
    </>
  );
};

export default Modal;

import React from "react";

const TopBar = () => {
  const currentTime = new Date();

  return (
    <div className="block mb-4">
      <div className="text-yujin-gray text-right">
        <p>
          {new Intl.DateTimeFormat("ID", {
            year: "numeric",
            month: "long",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
          }).format(currentTime)}
        </p>
      </div>
    </div>
  );
};

export default TopBar;

import React, { useState } from "react";
import Button from "../shared/UIElement/FormElements/Button";
import Input from "../shared/UIElement/FormElements/Input";
import Modal from "../shared/UIElement/Modal";

interface IResultProps {
  total: number;
  onConfirmPaymentHandler: (payValue: number) => void;
  onFinishPaymentHandler: () => void;
}

const Result: React.FC<IResultProps> = (props) => {
  const [userPayment, setUserPayment] = useState<number>(10);
  const [isPayOpen, setPayOpen] = useState<boolean>(false);

  const openPaymentHandler = () => setPayOpen(true);
  const closePaymentHandler = () => setPayOpen(false);

  const onPaymentValueChangeHandler = (id: string, value: string, valid: boolean) => {
    if (valid && value) {
      setUserPayment(parseInt(value!));
    }
  };
  const vaildatePaymentInput = (pay: number) => pay >= props.total;

  return (
    <>
      {isPayOpen && (
        <Modal
          onCancel={closePaymentHandler}
          show={isPayOpen}
          footer={
            <Button
              onClick={() => {
                props.onConfirmPaymentHandler(userPayment);
                closePaymentHandler();
              }}
              cssClasses={["bg-yujin-blue", "p-2 m-2", "float-right"]}
            >
              Ok
            </Button>
          }
        >
          <>
            <h4>Testing kaka</h4>
            <Input
              id="input-payment"
              element="input"
              type="number"
              label="Input Payment"
              errorText="The payment is insufficient"
              placeholder={props.total + ""}
              checkValidation={vaildatePaymentInput}
              onInput={onPaymentValueChangeHandler}
              isValid={false}
              value={'0'}
            />
          </>
        </Modal>
      )}
      <div className="container mx-auto mt-8 lg:px-3">
        <table className="w-full">
          <tbody>
            <tr className="text-yujin-blue">
              <td className="text-xl font-extrabold tracking-wider">Total</td>
              <td className="text-right text-xl font-extrabold tracking-wider">{props.total}</td>
            </tr>
            <tr className="text-yujin-red">
              <td className="text-lg font-bold tracking-wide">Pembayaran</td>
              <td className="text-right text-lg font-bold tracking-wide">
                <Button cssClasses={["m-0 p-0", "bg-transparent"]} onClick={openPaymentHandler}>
                  {userPayment}
                </Button>
              </td>
            </tr>
            {userPayment > props.total && (
              <tr className="text-sm text-gray-600">
                <td>Kembali</td>
                <td className="text-right">{userPayment - props.total}</td>
              </tr>
            )}
          </tbody>
        </table>
        <div className="my-4 mx-1 py-1">
          <div className="block w-fit mx-auto">
            <button
              className={[
                "px-6 py-1 mx-2 my-1",
                "bg-gray-400",
                "text-slate-50",
                "rounded-md",
                "text-md",
              ].join(" ")}
            >
              Discount
            </button>
          </div>
          <div className="block w-fit mx-auto">
            <button
              className={[
                "px-6 py-1 mx-2 my-1",
                "bg-yujin-blue",
                "rounded-md",
                "text-2xl text-slate-50",
                "font-semibold",
              ].join(" ")}
              onClick={props.onFinishPaymentHandler}
              disabled={userPayment < props.total}
            >
              Bayar
            </button>
          </div>
          <div className="block w-fit mx-auto">
            <button
              className={[
                "px-6 py-1 mx-2 my-1",
                "bg-yujin-blue",
                "rounded-md",
                "text-2xl text-slate-50",
                "font-semibold",
              ].join(" ")}
            >
              Print Receipt
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Result;

import React from "react";
import { Search } from "heroicons-react";

import AvailableItemChild from "./AvailableItemChild";
import { IItemContext } from "../../../utilities/contexts/item-context";

interface IAvailableItemProps {
  items: IItemContext[];

  onChildItemClick: (item: IItemContext) => void;
}

const AvailableItems: React.FC<IAvailableItemProps> = props => {
  const availableItems = props.items.filter(item => item.stock > 0);
  const notReadyItems = props.items.filter(item => item.stock <= 0);

  return (
    <>
      <div className="block mx-auto w-full my-4 relative text-yujin-gray ">
        <input
          className="w-full px-2 py-1 border border-yujin-gray rounded-md"
          placeholder="Search items"
        />

        <button className="bg-transparent border-none absolute top-0 right-0 py-1 px-2">
          <Search />
        </button>
      </div>
      <div className="mt-2 mb-8 grid grid-cols-4 gap-3 lg:grid-cols-6 lg:gap-4">
        {availableItems.map((x, index) => (
          <AvailableItemChild
            key={x.name + "-" + index}
            onClick={props.onChildItemClick}
            item={x}
            cssClasses={[
              "p-2",
              "shadow-md",
              "border-2 border-yujin-blue",
              "border-box",
              "text-center",
              "bg-slate-50 hover:bg-yujin-blue active:bg-yujin-blue",
              "text-yujin-blue hover:text-slate-50 active:text-slate-50",
            ]}
          ></AvailableItemChild>
        ))}
        
        {notReadyItems.map((x, index) => (
          <AvailableItemChild
            key={x.name + "-" + index}
            onClick={props.onChildItemClick}
            item={x}
            cssClasses={[
              "p-2",
              "shadow-md",
              "border-2 border-gray-300",
              "border-box",
              "text-center",
              "bg-slate-50 hover:bg-gray-300 active:bg-gray-300",
              "text-gray-300 hover:text-slate-50 active:text-slate-50",
            ]}
          ></AvailableItemChild>
        ))}
      </div>
    </>
  );
};

export default AvailableItems;

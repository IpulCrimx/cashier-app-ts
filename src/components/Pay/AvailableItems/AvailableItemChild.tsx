import React from "react";
import { IItemContext } from "../../../utilities/contexts/item-context";
import Card from "../../shared/UIElement/Card";

interface IAvailableChildProps {
  item: IItemContext;
  cssClasses?: string[];

  onClick?: (item: IItemContext) => void;
}

const AvailableItemChild: React.FC<IAvailableChildProps> = (props) => {
  const cardStyle: string[] = [];
  if (props.cssClasses) {
    cardStyle.push(...props.cssClasses);
    cardStyle.push(
      props.item.stock > 0 ? "cursor-pointer" : "cursor-not-allowed"
    );
  }

  return (
    <Card
      cssClasses={cardStyle}
      onClick={() => {
        if (props.onClick) props.onClick(props.item);
      }}
    >
      <h1 className="text-2lg font-extrabold tracking-wide">
        {props.item.name}
      </h1>
      <p className="text-sm font-light">stock: {props.item.stock}</p>
      <p className="text-md font-bold ">{props.item.price}</p>
    </Card>
  );
};

export default AvailableItemChild;

import React from "react";

import TableRow from "./TableRow";
import { RegisteredItemProps } from "../../pages/Pay";
import { IItemContext } from "../../utilities/contexts/item-context";

interface ITableParentProps {
  registeredItem?: RegisteredItemProps[];
  cssClasses?: string[];

  onChangeHandler: (itemName: string, price: number, quantity: number) => void;
  onRemoveHandler: (itemName: string) => void;
}

const TableParent: React.FC<ITableParentProps> = (props) => {
  const classes: string[] = ["lg:px3"];
  if (props.cssClasses) classes.push(...props.cssClasses);

  const onChangeQuantityHandler = (item: RegisteredItemProps, quantity: number) => {
    props.onChangeHandler(item.name, item.price, quantity);
  };

  return (
    <div className={classes.join(" ")}>
      <div className="text-4xl text-yujin-blue hover:text-yujin-blue-hover font-bold tracking-wider my-2">
        Receipt
      </div>
      <div className="border-b-2 border-gray-700 opacity-25 my-4"></div>
      <table className="w-full">
        <tbody>
          {props.registeredItem &&
            props.registeredItem.map((item) => (
              <TableRow
                key={item.name + "--" + item.price}
                item={item}
                onChange={onChangeQuantityHandler}
                onRemove={props.onRemoveHandler}
              />
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableParent;

import { FC } from "react";
import { MinusSm, PlusSm, X } from "heroicons-react";

import { IItemContext } from "../../utilities/contexts/item-context";
import { RegisteredItemProps } from "../../pages/Pay";

interface ITableRowProps {
  item: RegisteredItemProps;

  onChange: (item: RegisteredItemProps, quantity: number) => void;
  onRemove: (itemName: string) => void;
}

const TableRow: FC<ITableRowProps> = (props) => {
  const { name, price } = props.item;
  const quantity = props.item.quantity ? props.item.quantity : 0;
  const iconBtnStyles: string[] = ["my-auto", "rounded-md", "mx-1", "text-slate-50", "cursor-pointer"];

  const blueIcon: string[] = ["bg-yujin-blue", "hover:bg-yujin-blue-hover", "active:bg-yujin-blue-hover"];
  const redIcon: string[] = ["bg-yujin-red", "hover:bg-yujin-red-hover", "active:bg-yujin-red-hover"];

  const onAddItem = () => props.onChange(props.item, quantity + 1);
  const onMinusItem = () => props.onChange(props.item,  quantity - 1);

  return (
    <tr>
      <td className="flex text-right">
        <MinusSm className={[...iconBtnStyles, ...redIcon].join(" ")} size={18} onClick={onMinusItem} />
        {quantity}
        <PlusSm className={[...iconBtnStyles, ...blueIcon].join(" ")} size={18} onClick={onAddItem} />
      </td>
      <td className="min-w-[50%]">{name}</td>
      <td className="flex justify-end">
        {quantity * price}
        <X
          className={[...iconBtnStyles, ...redIcon].join(" ") + " ml-2"}
          size={18}
          onClick={() => {
            props.onRemove(name);
          }}
        />
      </td>
    </tr>
  );
};

export default TableRow;

import React from "react";

import dummy_log from "./json/dummy_log.json";

interface IChildProps {
  info: string;
  user: string;
  cssClasses?: string[];
}

const AnalyticLog = () => {
  const listStyle: string[] = [
    "p-2",
    "text-xs",
    "border border-yujin-blue",
    "bg-yujin-bg-blue",
    "text-slate-600",
    "rounded-md",
  ];
  const oddStyle: string[] = [];
  const evenStyle: string[] = [];

  const sorted = dummy_log.sort((x, y) => {
    if (x.datetime > y.datetime) return -1;
    else if (x.datetime < y.datetime) return 1;
    else return 0;
  });

  return (
    <div className={listStyle.join(" ")}>
      <h1 className="text-lg text-yujin-blue font-bold">Last Update</h1>
      <ul>
        {sorted.map((item, index) => (
          <AnalyticChild
            key={index + "--" + item.user + "-" + item.item}
            info={`${item.datetime} - ${item.item}  ditambahkan oleh `}
            user={item.user}
            cssClasses={index % 2 === 0 ? evenStyle : oddStyle}
          />
        ))}
      </ul>
    </div>
  );
};

const AnalyticChild: React.FC<IChildProps> = (props) => {
  return (
    <li className={props.cssClasses?.join(" ")}>
      {props.info}
      <span className="text-green-400">{props.user}</span>
    </li>
  );
};

export default AnalyticLog;

import React from "react";

import dummy_info from "./json/dummy_iteminfo.json";

interface ILowItemProps {
  message: string;
  threshold: number;
  cssClasses?: string[];
}

const LowItemInfo = () => {
  const threshold: number = 20;
  const styles: string[] = [
    "w-full",
    "p-2",
    "border border-yujin-red",
    "rounded-md",
    "text-xs",
  ];

  const childStyle: string[] = ["text-slate-900"];
  const childDangerStyle: string[] = ["text-red-700"];

  return (
    <div className={styles.join(" ")}>
      <h1 className="text-lg text-yujin-red font-bold">Low on Stock</h1>
      <ul className="list-disc mx-4">
        {dummy_info
          .filter((x) => x.stock < threshold)
          .slice(0, 5)
          .map((item, index) => (
            <LowItemChild
              key={item.item + "-" + index}
              threshold={item.stock < 5 ? 5 : threshold}
              message={item.item + " quantity are"}
              cssClasses={item.stock < 5 ? childDangerStyle : childStyle}
            />
          ))}
      </ul>
    </div>
  );
};

const LowItemChild: React.FC<ILowItemProps> = (props) => {
  return (
    <li className={props.cssClasses?.join(" ")}>
      {props.message + " < " + props.threshold}
    </li>
  );
};

export default LowItemInfo;

import { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Navigation from "./components/shared/navigation/Navigation";
import Analytic from "./pages/Analytic";
import Homepage from "./pages/Homepage";
import ItemListPage from "./pages/ItemListPage";
import Login from "./pages/Login";
import Pay from "./pages/Pay";

import "./App.css";
import dummy_item from "./constants/dummy_item.json";
import { IItemContext, ItemContext } from "./utilities/contexts/item-context";
import { AuthContext } from "./utilities/contexts/auth-context";
import { Logout } from "heroicons-react";

function App() {
  const [isLoggedIn, setLoggedIn] = useState<boolean>(false);
  const [isAdmin, setIsAdmin] = useState<boolean>(false);
  const [dummyItems, setDummyItem] = useState<IItemContext[]>(dummy_item);

  const login = (success: boolean, admin: boolean) => {
    setLoggedIn(success);
    setIsAdmin(admin);
  };

  const logout = () => {
    setLoggedIn(false);
    setIsAdmin(false);
  };

  let routes = {};
  if (isLoggedIn) {
    if (isAdmin) {
      routes = (
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/main" element={<Pay />} />
          <Route path="/items" element={<ItemListPage />} />
          <Route path="/graph" element={<Analytic />} />
          <Route path="/logout" element={<Logout />} />
        </Routes>
      );
    } else {
      routes = (
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/main" element={<Pay />} />
          <Route path="/items" element={<ItemListPage />} />
          <Route path="/logout" element={<Logout />} />
        </Routes>
      );
    }
  } else {
    routes = (
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/*" element={<Homepage />} />
      </Routes>
    );
  }

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ isLoggedIn, isAdmin, login, logout }}>
        <Navigation />
        <main style={{ paddingTop: "3.5rem" }}>
          <ItemContext.Provider value={{ items: dummyItems, setItems: setDummyItem }}>
            {routes}
          </ItemContext.Provider>
        </main>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;

module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      minWidth: {
        "1/6": "16%",
        "1/4": "25%",
        "1/3": "33%",
        "1/2": "50%",
        "3/4": "75%",
        1: "100%",
      },
      maxWidth: {
        "1/6": "16%",
        "1/4": "25%",
        "1/3": "33%",
        "1/2": "50%",
        "3/4": "75%",
        1: "100%",
      },
      width: {
        "3/10": "30%",
        "7/10": "70%",
      },
      colors: {
        "yujin-lightblue": "#EEFBFF",
        "yujin-blue": "#71BAE1",
        "yujin-blue-hover": "#5690A5",
        "yujin-red": "#D87373",
        "yujin-red-hover": "#994A4A",
        "yujin-gray": "#A7A9AC",
        "regal-blue": "#243c5a",
      },
    },
  },
  plugins: [],
};
